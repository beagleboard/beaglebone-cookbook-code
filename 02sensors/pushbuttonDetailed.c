////////////////////////////////////////
//	pushbutton.c
//      Reads P9_42 and prints its value.
//	Wiring:	Connect a switch between P9_42 and 3.3V
//	Setup:	
//	See:	
////////////////////////////////////////
#include <gpiod.h>
#include <stdio.h>
#include <unistd.h>

#define CONSUMER        "pushbutton.c"

int main(int argc, char **argv)
{
        int chipnumber = 0;
        unsigned int line_num = 7;
        struct gpiod_line *line;
        struct gpiod_chip *chip;
        int i, ret;

        chip = gpiod_chip_open_by_number(chipnumber);
        if (!chip) {
                perror("Open chip failed\n");
                goto end;
        }

        line = gpiod_chip_get_line(chip, line_num);
        if (!line) {
                perror("Get line failed\n");
                goto close_chip;
        }

        ret = gpiod_line_request_input(line, CONSUMER);
        if (ret < 0) {
                perror("Request line as intput failed\n");
                goto release_line;
        }

        /* Get */
        while(1) {
                printf("%d\r", gpiod_line_get_value(line));
                usleep(1000);
        }

release_line:
        gpiod_line_release(line);
close_chip:
        gpiod_chip_close(chip);
end:
        return 0;
}


// int ms = 500;    // Read time in ms
// int CHIP = 0;
// LINE_OFFSET = [7] #  P9_42 is gpio 7
// chip = gpiod.Chip(CHIP)
// lines = chip.get_lines(LINE_OFFSET)
// lines.request(consumer='pushbutton.py', type=gpiod.LINE_REQ_DIR_IN)

// while True:
//     data = lines.get_values()
//     print('data = ' + str(data[0]))
//     time.sleep(ms/1000)
